from django.shortcuts import render
from .models import *

# Create your views here.
def index(request):
    grupo1 = Estudiante.objects.filter(grupo = 1)
    grupo4 = Estudiante.objects.filter(grupo = 4)
    mismoApellido = Estudiante.objects.filter(apellidos = "Mendoza")
    mismaEdad = Estudiante.objects.filter(edad = 22)
    mismoGrupoEdad = Estudiante.objects.filter(grupo = 3, edad = 22)
    estudiantes = Estudiante.objects.all()

    return render(request, 'index.html', {'grupo1': grupo1, 
                                          'grupo4': grupo4, 
                                          'mismoApellido': mismoApellido, 
                                          'mismaEdad': mismaEdad,
                                          'mismoGrupoEdad': mismoGrupoEdad,
                                          'estudiantes': estudiantes})